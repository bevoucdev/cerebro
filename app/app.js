'use strict';

/**
 * @ngdoc overview
 * @name cerebroApp
 * @description
 * # cerebroApp
 *
 * Main module of the application.
 */
var mainApp = angular.module('cerebroApp', ['ngAnimate',
                                            'ngCookies',
                                            'ngResource',
                                            'ngRoute',
                                            'ngSanitize',
                                            'ngTouch',
                                          ]);

  mainApp.config(function ($routeProvider, $locationProvider) {
    $locationProvider.hashPrefix('!');
    $routeProvider
      .when('/', {
        templateUrl:'components/signIn/signInView.html',
        controller:'SignInController'
      })
      .when('/datatable', {
        templateUrl: 'components/table/tableView.html'/*,
        controller: 'TableController'*/
      })
      .when('/filesmanager', {
        templateUrl: 'components/filesManager/filesManagerView.html',
        controller: 'FilesManagerController'
      })
      .when('/sidebar', {
        templateUrl: 'components/sidebar/sidebarView.html'
        //controller: 'SidebarController'
      })
      .when('/error', {
        templateUrl: '404.html'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
