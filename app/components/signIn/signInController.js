// Client ID and API key from the Developer Console
var CLIENT_ID = '923057939690-aas0soah5tko85tg5pas680dqvsdc9op.apps.googleusercontent.com';
var API_KEY = 'AIzaSyB2AuQ3z9PclAbd7nXgJsgIbeh3x12KZ2g';

// Array of API discovery doc URLs for APIs used by the quickstart
var DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/drive/v3/rest", "https://sheets.googleapis.com/$discovery/rest?version=v4", "https://slides.googleapis.com/$discovery/rest?version=v1"];

// Authorization scopes required by the API; multiple scopes can be
// included, separated by spaces.
var SCOPES = 'https://www.googleapis.com/auth/drive.metadata.readonly https://www.googleapis.com/auth/spreadsheets.readonly https://www.googleapis.com/auth/presentations.readonly';

var handleLoadClient = function()
{
  gapi.load('auth2', function()
  {
    gapi.client.init({
          apiKey: API_KEY,
          clientId: CLIENT_ID,
          discoveryDocs: DISCOVERY_DOCS,
          scope: SCOPES
    });
  });
}

mainApp.controller('SignInController', function($scope, $location) {

    //load in the auth2 api's, without it gapi.auth2 will be undefined
    //gapi.auth2.init({client_id: CLIENT_ID});
  handleLoadClient();

  $scope.onLogInButtonClick = function(event)
  {//add a function to the controller so ng-click can bind to it
    var GoogleAuth  = gapi.auth2.getAuthInstance();//get's a GoogleAuth instance with your client-id, needs to be called after gapi.auth2.init
    GoogleAuth.signIn().then(function(response)
    {
      $scope.$apply(function(){$location.path('/sidebar');});
    });
  };

});
