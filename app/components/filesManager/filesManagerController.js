// Client ID and API key from the Developer Console
var CLIENT_ID = '923057939690-aas0soah5tko85tg5pas680dqvsdc9op.apps.googleusercontent.com';
var API_KEY = 'AIzaSyB2AuQ3z9PclAbd7nXgJsgIbeh3x12KZ2g';

// Array of API discovery doc URLs for APIs used by the quickstart
var DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/drive/v3/rest", "https://sheets.googleapis.com/$discovery/rest?version=v4", "https://slides.googleapis.com/$discovery/rest?version=v1"];

// Authorization scopes required by the API; multiple scopes can be
// included, separated by spaces.
var SCOPES = 'https://www.googleapis.com/auth/drive.metadata.readonly https://www.googleapis.com/auth/spreadsheets.readonly https://www.googleapis.com/auth/presentations.readonly';

var clients_array = [];

var mainApp = angular.module('cerebroApp');

var hideSigninButton = function(isSignedIn)
{
  var x = document.getElementById('signin_button');
  if (isSignedIn) {
      x.style.display = 'none';
  } else {
      x.style.display = 'block';
  }
}

var createSlide = function(presentationId)
{
  var requests = [{
    // Creates a "TITLE_AND_BODY" slide with objectId references
    createSlide: {
      objectId: "1",
      slideLayoutReference: {
        predefinedLayout: 'TITLE_AND_BODY'
      },
      placeholderIdMappings: [{
        layoutPlaceholder: {
          type: 'TITLE'
        },
        objectId: "1"
      }, {
        layoutPlaceholder: {
          type: 'BODY'
        },
        objectId: "1"
      }]
    }
  }];

// If you wish to populate the slide with elements, add element create requests here,
// using the pageId.

// Execute the request.
gapi.client.slides.presentations.batchUpdate({
presentationId: presentationId,
requests: requests
}).then((createSlideResponse) => {
console.log(`Created slide with ID: ${createSlideResponse.result.replies[0].createSlide.objectId}`);
});
}

var writeOnSlide = function(presentationId, text_to_insert)
{
  console.log("try to write on slide: ")
  var requests = [{
     insertText:{
       objectId: "1",
       text: text_to_insert,
       insertionIndex: 0
     }
  }];

  // Execute the request.
  gapi.client.slides.presentations.batchUpdate({
  presentationId: presentationId,
  requests: requests
}).then((writeOnSlideResponse) => {
  console.log(writeOnSlideResponse/*`Write on slide with ID: ${writeOnSlideResponse.result.replies[0].createSlide.objectId}`*/);
  });
}

var createPresentation= function(i)
{/*
  var title_slide = i.toString();
  gapi.client.slides.presentations.create({
    title: title_slide
    slides: [
    {

    }
  ],
  }).then((response) => {
    console.log(`Created presentation with ID: ${response.result.presentationId}`);
  });*/
}

var getPresentation = function()
{
  gapi.client.slides.presentations.get(presentationId).then((response) =>{
    console.log("Got the presentation ID: " + response.result.presentationId);
  });
}

var showContent = function()
{
  var x = document.getElementById('main_content');
  if (x.style.display === 'block') {
      x.style.display = 'none';
  } else {
      x.style.display = 'block';
  }
}

var appendPre = function(message) {
        var pre = document.getElementById('content');
        var textContent = document.createTextNode(message + '\n');

        if(typeof pre != "undefined")
        {
            pre.appendChild(textContent);
        }
      }

      /**
       * Print the names and majors of students in a sample spreadsheet:
       * https://docs.google.com/spreadsheets/d/1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms/edit
       */
var listSheetContent = function() {
  var params = {
  // The spreadsheet to request.
  spreadsheetId: '1nuhwkFAEapedQADS2dSu0NYEwJ-RrzG6H9gh_ePm4mk',  // TODO: Update placeholder value.

  // The ranges to retrieve from the spreadsheet.
  range: 'Input',  // TODO: Update placeholder value.

  majorDimension: 'ROWS',

  valueRenderOption: 'FORMATTED_VALUE',

  // True if grid data should be returned.
  // This parameter is ignored if a field mask was set in the request.
  //includeGridData: true,  // TODO: Update placeholder value.
  };

  var request = gapi.client.sheets.spreadsheets.values.get(params);
  request.then(function(response) {
    // TODO: Change code below to process the `response` object:
    //console.log(response.result.values);

    var tab = [];

    for(i = 0; i < response.result.values.length; ++i)
    {
      var id = createPresentation(i);

      tab = [];

      for(j = 0; j < response.result.values[i].length; j+=4)
      {
        var row = response.result.values[i];

        tab.push(row[j]);
/*
        gapi.client.slides.presentations.get(id).then(function(callback){
          console.log("trouvé: " + id);
        })*/
        //createSlide(id);
        //writeOnSlide(id, "ligne " + i + " : " + row[j] + ', ' + row[j+1] + ', ' + row[j+2] + ', ' + row[j+3])
        //console.log(response.result.values[i][j]);
      }

      appendPre(tab);
    }
  }, function(reason) {
    console.error('error: ' + reason.result.error.message);
    });
}

var handleLoadClient = function()
{
  gapi.load('auth2', function()
  {
    gapi.client.init({
          apiKey: API_KEY,
          clientId: CLIENT_ID,
          discoveryDocs: DISCOVERY_DOCS,
          scope: SCOPES
        }).then(function () {
          // Listen for sign-in state changes.
          gapi.auth2.getAuthInstance().isSignedIn.listen(hideSigninButton);

          // Handle the initial sign-in state.
          //hideSigninButton(gapi.auth2.getAuthInstance().isSignedIn.get());
        });
  });
}

mainApp.controller('FilesManagerController', function($scope) {

    //load in the auth2 api's, without it gapi.auth2 will be undefined
    //gapi.auth2.init({client_id: CLIENT_ID});

  handleLoadClient();
  $scope.onLogInButtonClick = function(event)
  {//add a function to the controller so ng-click can bind to it
    var GoogleAuth  = gapi.auth2.getAuthInstance();//get's a GoogleAuth instance with your client-id, needs to be called after gapi.auth2.init
    GoogleAuth.signIn().then(function(response)
    {//request to sign in
      listSheetContent();
      //makeApiCall();
    });
  };
});
